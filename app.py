import os
from flask import Flask, request
import numpy as np
import scipy.optimize as opt
from thermoengine import core, phases, model, equilibrate
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=FutureWarning)

src_obj = core.get_src_object('EquilibrateUsingMELTSv102')
modelDB = model.Database(liq_mod='v1.0')
Liquid = modelDB.get_phase('Liq')
Feldspar = modelDB.get_phase('Fsp')
Quartz = modelDB.get_phase('Qz')
Water = phases.PurePhase('WaterMelts', 'H2O', calib=False)

elm_sys = ['H','O','Na','Mg','Al','Si','P','K','Ca','Ti','Cr','Mn','Fe','Co','Ni']
phs_sys = [Liquid, Water]
equil = equilibrate.Equilibrate(elm_sys, phs_sys)

state = None

def wt_oxides_to_elements(oxide_d):
	tot_grm_oxides = 0.0
	for key in oxide_d.keys():
		tot_grm_oxides += oxide_d[key]
	mol_oxides = core.chem.format_mol_oxide_comp(oxide_d, convert_grams_to_moles=True)
	moles_end,oxide_res = Liquid.calc_endmember_comp(
		mol_oxide_comp=mol_oxides, method='intrinsic', output_residual=True)
	if not Liquid.test_endmember_comp(moles_end):
		print ("Calculated composition is infeasible!")
	mol_elm = Liquid.covert_endmember_comp(moles_end,output='moles_elements')
	blk_cmp = []
	for elm in elm_sys:
		index = core.chem.PERIODIC_ORDER.tolist().index(elm)
		blk_cmp.append(mol_elm[index])
	return np.array(blk_cmp)

def affinity(x, blk_cmp, NNO_offset, doprint=False, return_for_scipy=True):
	global state, equil, Feldspar, Quartz
	t = x[0] 
	p = x[1]
	if state is None:
		state = equil.execute(t, p, bulk_comp=blk_cmp, con_deltaNNO=NNO_offset, debug=0, stats=False)
	else:
		state = equil.execute(t, p, state=state, con_deltaNNO=NNO_offset, debug=0, stats=False)
	if doprint:
		state.print_state()
	muLiq = state.dGdn(t=t, p=p, element_basis=False)[:,0]
	result = {}
	muFld = np.array([
		5.0*muLiq[0]/2.0 + muLiq[2]/2.0 + muLiq[11]/2.0,
			muLiq[0] + muLiq[2] + muLiq[10],
		2.0*muLiq[0] + muLiq[12]
	])
	muQtz = np.array([muLiq[0]])
	result['Feldspar'] = Feldspar.affinity_and_comp(t, p, muFld, method='special')
	result['Quartz']   = Quartz.affinity_and_comp(t, p, muQtz, method='special')
	if return_for_scipy:
		AffnFld = result['Feldspar'][0]
		AffnQtz = result['Quartz'][0]
		sumsq = (AffnFld/13.0)**2 + (AffnQtz/3.0)**2
		return sumsq
	else:
		return result

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def gebarometer():
	if request.method == 'POST':
		grm_oxides = {
		'SiO2':  float(request.form['SiO2']), 
		'TiO2':  float(request.form['TiO2']), 
		'Al2O3': float(request.form['Al2O3']), 
		'Fe2O3': float(request.form['Fe2O3']),
		'Cr2O3': float(request.form['Cr2O3']), 
		'FeO':   float(request.form['FeO']), 
		'MnO':   float(request.form['MnO']),
		'MgO':   float(request.form['MgO']), 
		'NiO':   float(request.form['NiO']), 
		'CoO':   float(request.form['CoO']),
		'CaO':   float(request.form['CaO']), 
		'Na2O':  float(request.form['Na2O']), 
		'K2O':   float(request.form['K2O']), 
		'P2O5':  float(request.form['P2O5']),
		'H2O':   10.0
		}
		NNO_offset = float(request.form['fO2 offset'])
	elif request.method == 'GET':
		grm_oxides = {
		'SiO2':  float(request.args.get('SiO2', '')), 
		'TiO2':  float(request.args.get('TiO2', '')), 
		'Al2O3': float(request.args.get('Al2O3', '')), 
		'Fe2O3': float(request.args.get('Fe2O3', '')),
		'Cr2O3': float(request.args.get('Cr2O3', '')), 
		'FeO':   float(request.args.get('FeO', '')), 
		'MnO':   float(request.args.get('MnO', '')),
		'MgO':   float(request.args.get('MgO', '')), 
		'NiO':   float(request.args.get('NiO', '')), 
		'CoO':   float(request.args.get('CoO', '')),
		'CaO':   float(request.args.get('CaO', '')), 
		'Na2O':  float(request.args.get('Na2O', '')), 
		'K2O':   float(request.args.get('K2O', '')), 
		'P2O5':  float(request.args.get('P2O5', '')),
		'H2O':   10.0
		}
		NNO_offset = float(request.args.get('fO2 offset', ''))

	blk_cmp = wt_oxides_to_elements(grm_oxides)
	t = 1034.0
	p = 1750.0
	state = None
	result = opt.minimize(affinity, 
		np.array([t, p]), 
		args=(blk_cmp, NNO_offset),
		options={'disp':False, 'xatol':1.0, 'fatol':1.0, 'return_all':False},
		method='Nelder-Mead'
		)

	return {
		"function": result.fun,
		"temperature": result.x[0]-273.15,
		"pressure": result.x[1]/10.0,
	}

if __name__ == "__main__":
	app.run(debug=True,host='0.0.0.0',port=int(os.environ.get('PORT', 8080)))
